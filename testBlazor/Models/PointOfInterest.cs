﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testBlazor.Models
{
    public class PointOfInterest
    {
        public int Id { get; set; }
        public string Name_fr { get; set; }
        public string Name_nl { get; set; }
        public string Name_en { get; set; }
        public string Image { get; set; }
        public string Description_fr { get; set; }
        public string Description_en { get; set; }
        public string Description_nl { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Interval { get; set; }
        public bool IsDeleted { get; set; }
        public int Category_id { get; set; }
    }
}
